'''
Submit malware sample for analysis to a Cincan pipeline.

'''
from pymisp import PyMISP
import json
import base64
import io
import zipfile
from git import Repo

misperrors = {'error': 'Error'}
mispattributes = {'input': ['attachment', 'malware-sample'], 'output': ['freetext']}
moduleinfo = {'version': '0.1', 'author': 'Juha Kalkainen',
              'description': 'Submit a sample to a Cincan malware analysis pipeline',
              'module-type': ['expansion']}

moduleconfig = ['path_to_repository']

def handler(q=False):
    if q is False:
        return False
    request = json.loads(q)

    try:

        data = request.get("data")
        if 'malware-sample' in request:
            sample_filename = request.get("malware-sample").split("|", 1)[0]
            data = base64.b64decode(data)
            fl = io.BytesIO(data)
            zf = zipfile.ZipFile(fl)
            sample_hashname = zf.namelist()[0]
            malware = zf.read(sample_hashname, b"infected")
            zf.close()

        elif 'attachment' in request:
            sample_filename = request.get("attachment")
            data = base64.b64decode(data)

        else:
            misperrors['error'] = "No relevant sample or attachment"
            return misperrors 

    except Exception as e:
        misperrors['error'] = "Error in handling submitted data. Error msg: %s"%(e)
        return misperrors

    if (request["config"].get("path_to_repository") is None):
        misperrors["error"] = "Path to Cincan analysis repo not set up."

    if data and sample_filename:
        args = {}
        args["sample_file"] = {'data': io.BytesIO(data), 'filename': sample_filename}

    try:
        path = "%s%s"%(request["config"].get("path_to_repository"),sample_filename)
        
        write_file(path, io.BytesIO(data))
        upload_to_git(path, io.BytesIO(data)):
        output = "https://172.20.0.5:5443"
        return {'results': [{'categories': ['External analysis'], 'types': ['url'], 'values': output, 'comment': "External analysis perforemd by cincan pipeline"}]}
    except Exception as e:
        misperrors['error'] = "Error uploading to git. Error msg: %s"%(e)
        return misperrors


def introspection():
    return mispattributes

def version():
    moduleinfo['config'] = moduleconfig
    return moduleinfo


def upload_to_git(repo, file):

    repo = Repo(repo)
    assert not repo.bare
    repo.git.add(A=True)
    repo.index.commit("%s uploaded for analysis" % (file))
    origin = repo.remote(name='origin')
    origin.pull
    origin.push()

def write_file(filename, data):
    json_file=open(filename, 'wb')
    json_file.write(data.read())
    json_file.close()
