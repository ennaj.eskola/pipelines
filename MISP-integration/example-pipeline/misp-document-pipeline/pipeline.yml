---
resources:
- name: sample-source
  type: git
  source:
    uri: ((keys.git-uri))
    branch: sample-source
    private_key: ((keys.private-key))
- name: results
  type: git
  source:
    uri: ((keys.git-uri))
    branch: results
    private_key: ((keys.private-key))
- name: work
  type: git
  source:
    uri: ((keys.git-uri))
    branch: master
    private_key: ((keys.private-key))

# Peepdf configuration
task-config: &peepdf-config
  platform: linux
  image_resource:
    type: registry-image
    source: {repository: cincan/peepdf}

# Pdfid configuration, also used for updating git
task-config: &pdfid-git-config
  platform: linux
  image_resource:
    type: registry-image
    source: {repository: cincan/pdfid}


# Sort files
jobs:
- name: job-sort-files
  plan:
  - get: sample-source
    trigger: true
  - get: work
  - get: results
  - task: sort-files-task
    config:
      << : *peepdf-config
      inputs:
        - name: sample-source
        - name: work
        - name: results
      outputs:
        - name: updated-source
        - name: updated-results
      run:
        path: /bin/bash
        args:
          - work/sort.sh
  - put: sample-source
    params: 
      repository: updated-source
      rebase: true
      force: true

  # Clear old logs from "results"
  - task: clear-logs
    config:
      << : *pdfid-git-config
      inputs:
        - name: results
        - name: work
        - name: updated-results
      outputs:
        - name: update-git
      run:
         path: /bin/sh
         args:
         - -c
         - |
           #!/bin/sh
           ls -lR
           git clone results update-git
           rm -rf update-git/*
           cp -r updated-results/filetypes.log update-git
           cd update-git
           git config --global user.email "cincan@cincan.io"
           git config --global user.name "Cincan"
           git add .
           git commit -m "sort files, clear logs"
           git pull --rebase
           ls -lR
    on_success:
      try:
        do:
        - task: task-success
          config:
            << : *pdfid-git-config
            run:
              path: echo
              args: ["Sorting succeeded"]
    on_failure:
      try:
        do:
        - task: task-failure
          config:
            << : *pdfid-git-config
            run:
              path: echo
              args: ["Failure, no samples to analyse?"]
  - put: results
    params: 
      repository: update-git
      merge: true
      force: true


# ClamAV Scan
- name: job-clamscan
  plan:
  - get: sample-source
    passed: [job-sort-files]
    trigger: true
  - get: work
    passed: [job-sort-files]
  - get: results
  - task: clamscan-task
    config:
      platform: linux
      image_resource:
        type: docker-image
        source: {repository: cincan/clamscan}
      inputs:
      - name: sample-source
      - name: work
      - name: results
      outputs:
      - name: output-files
      run:
        path: sh
        args:
          - work/clamscan.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git/
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update clamscan results"
          git pull --rebase 
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      

# PDF Pipeline
# Pdfid
- name: job-pdfid
  plan:
  - get: sample-source
    passed: [job-clamscan]
    trigger: true
  - get: work
    trigger: true
  - get: results
  - task: pdfid-task
    config:
      << : *pdfid-git-config
      inputs:
      - name: sample-source
      - name: work
      - name: results
      outputs:
      - name: output-files
      run:
        path: sh
        args:
          - work/pdfid.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update pdfid results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false

# Peepdf
- name: job-peepdf-virustotal-check
  plan:
  - get: sample-source
    passed: [job-clamscan]
    trigger: true
  - get: results
  - get: work
    trigger: true
  - task: peepdf-virustotal-task
    config:
      << : *peepdf-config
      inputs:
        - name: sample-source
        - name: results
        - name: work
      outputs:
        - name: output-files
      run:
        path: /bin/bash
        args:
          - work/peepdf-vt.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update peepdf results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      

# Jsunpack-n
- name: job-jsunpackn
  plan:
  - get: results
    passed: [job-peepdf-virustotal-check]
    trigger: true
  - get: work
    passed: [job-pdfid]
    trigger: true
  - task: jsunpackn-task
    config:
      platform: linux
      image_resource:
        type: docker-image
        source: {repository: cincan/jsunpack-n}
      inputs:
      - name: results
      - name: work
      outputs:
      - name: output-files
      run:
        path: /bin/bash
        args:
          - work/jsunpackn.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          # Remove "sample" folder after jsunpack-n analysis
          rm -rf sample
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update jsunpack-n results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      

# Sctest
- name: job-sctest
  plan:
  - get: results
    passed: [job-jsunpackn]
    trigger: true
  - get: work
    passed: [job-jsunpackn]
    trigger: true
  - task: sctest-task
    config:
      << : *peepdf-config
      inputs:
        - name: results
        - name: work
      outputs:
        - name: output-files
      run:
        path: /bin/bash
        args:
          - work/sctest.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update sctest results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      

# Document analysis
# Oledump
- name: job-oledump
  plan:
  - get: sample-source
  - get: work
  - get: results
    passed: [job-strings]
    trigger: true
  - task: oledump-task
    config:
      platform: linux
      image_resource:
        type: docker-image
        source: {repository: cincan/oledump-linux}
      inputs:
      - name: sample-source
      - name: work
      outputs:
      - name: output-files
      run:
        path: sh
        args:
          - work/oledump.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update oledump results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      

# Olevba
- name: job-olevba
  plan:
  - get: sample-source
    passed: [job-oledump]
  - get: work
  - get: results
    passed: [job-oledump]
    trigger: true
  - task: olevba-task
    config:
      platform: linux
      image_resource:
        type: docker-image
        source: {repository: cincan/olevba}
      inputs:
      - name: sample-source
      - name: work
      outputs:
      - name: output-files
      run:
        path: sh
        args:
          - work/olevba.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update olevba results"
          git pull --rebase 
  - put: results
    params: 
      repository: update-git
      merge: true
      force: false
      
## Strings
- name: job-strings
  plan:
  - get: sample-source
    passed: [job-sort-files]
    trigger: true
  - get: work
  - get: results
  - task: strings-task
    config:
      << : *pdfid-git-config
      inputs:
      - name: sample-source
      - name: work
      - name: results
      outputs:
      - name: output-files
      run:
        path: sh
        args:
          - work/strings.sh
  - task: update-git
    config:
      << : *pdfid-git-config
      inputs:
      - name: output-files
      - name: results
      outputs:
      - name: update-git
      run:
        path: /bin/sh
        args:
        - -c
        - |
          #!/bin/sh
          ls -R
          git clone results update-git
          cp -r output-files/* update-git
          cd update-git
          git config --global user.email "cincan@cincan.io"
          git config --global user.name "Cincan"
          git add .
          git commit -m "update strings results"
          git pull --rebase
  - put: results
    params: 
      repository: update-git
      merge: true
      force: true

- name: job-upload-misp
  plan:
  - get: sample-source
    passed: [job-sort-files]
    trigger: true
  - get: work
    passed: [job-sort-files]
    trigger: true
  - task: task-upload-misp
    config:
      platform: linux
      image_resource:
        type: docker-image
        source: {repository: rloota/pymisp-pipeline-test}
      inputs:
        - name: sample-source
        - name: work        
      params:
        mispkey: ((keys.mispkey))
        mispaddress: ((keys.mispaddress))
      run:
        path: sh
        user: root
        args:
        - -exc
        - |
          mispkey=((keys.mispkey))
          mispaddress=((keys.mispaddress))
          work/MISP_upload.sh
