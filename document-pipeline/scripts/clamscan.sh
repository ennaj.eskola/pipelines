#!/bin/sh
# Scan all files with ClamAV
SAMPLESPATH=$(pwd)
ls "$SAMPLESPATH"/sample-source/ -R
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info
CLAMAV_JSON_PATH=$SAMPLESPATH/output-files/json-output/clamav
mkdir -p "$CLAMAV_JSON_PATH"
mkdir -p /tmp/clamav/

# Do nothing if folder is empty, otherwise loop through samples.
number_of_files=$(find "$SAMPLESPATH"/sample-source | wc -l)
if [ "$number_of_files" = 0 ]; then
    echo "Folder is empty"
    date > "$SAMPLESPATH"/output-files/clamscan-empty.log
else

    
    # UBUNTU version stuff
    # apt install --only-upgrade -y clamav

    # Update virus database
    echo "Updating ClamAv databases"
    freshclam
    # Get version information
    /usr/bin/clamscan -V >"$SAMPLESPATH"/output-files/tool-version-info/clamav
    echo "Processing files"
    # Scan the files

    # We don't want want use --recursive mode here, since we need to add extra metadata about files
    # However, if we scan syncronously one by one, it takes a lot of time
    # ClamAV reloads signatures each time it starts if running without daemon
    # We can't use daemon, as it is missing --gen-json option :(

    # EDIT using recursive after all, but manually finding filenames and sha256 hashes by comparing md5 hashes...
    # NOT best way

    add_metadata() {
        file="$1"
        json_file="$2"
        hash=$(sha256sum "${file}" | cut -d ' ' -f1)
        name=$(basename "${file}")
        # Add SHA256 Hash and filename for ClamAV result JSON and pretty print it as well
        # File extension changed from .tmp to .json
        jq --arg hash "$hash" \
           --arg name "$name" \
           '.+ {fileSHA256: $hash, fileName: $name}' "$json_file" \
           >"$CLAMAV_JSON_PATH"/clamav_"$hash".json
           
        echo "Metadata added for file $file."
    }

    /usr/bin/clamscan -zr "$SAMPLESPATH"/sample-source/* --exclude="$SAMPLESPATH"/sample-source/.git/ --gen-json --leave-temps --tempdir=/tmp/clamav/ --no-summary
    # Get only JSON output 
    mkdir -p /tmp/clamav/json/
    file /tmp/clamav/*.tmp | grep JSON | cut -d ':' -f1 | xargs -I '{}' mv {} /tmp/clamav/json/

    find "$SAMPLESPATH"/sample-source/* -type f -not -path "$SAMPLESPATH/sample-source/.git*/" | while IFS= read -r sample; do
        # Read clamav generated md5, and based on that identify filename and generate sha256 hash
        md5hash=$(md5sum "${sample}" | cut -d ' ' -f1)
        for f in /tmp/clamav/json/*.tmp; do
            clamav_md5=$(jq -r ".FileMD5" "$f")
            # echo "MD5 IS $clamav_md5"
            if [ "$clamav_md5" = "$md5hash" ]; then
                add_metadata "$sample" "$f" 
            fi
        done
    done

    date > "$SAMPLESPATH"/output-files/_timestamp_
fi
