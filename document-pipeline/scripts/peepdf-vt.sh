#!/bin/bash
SAMPLESPATH=$(pwd)
find "$SAMPLESPATH"/sample-source/pdf/ -type f  ! -path "$SAMPLESPATH/sample-source/.git*"

# Folders for sorting files: found on Virustotal or not.
mkdir -p "$SAMPLESPATH"/virustotal
mkdir -p "$SAMPLESPATH"/samples
mkdir -p "$SAMPLESPATH"/json-output/peepdf
mkdir -p "$SAMPLESPATH"/output-files/tool-version-info

# Do nothing if folder is empty, otherwise loop through samples.
if (("$(find "$SAMPLESPATH"/sample-source/pdf/ | wc -l)" == 0)); then
	echo "Folder is empty"
    date >"$SAMPLESPATH"/output-files/peepdf-empty.log
else
	echo "Processing files"
	cd /peepdf || exit
	# Get version information
	/usr/bin/python peepdf.py -v | grep Version |  cut -d " " -f2,3,4 > "$SAMPLESPATH"/output-files/tool-version-info/peepdf

	amount="$(find "$SAMPLESPATH"/sample-source/pdf/ -type f  ! -path "$SAMPLESPATH/sample-source/.git*" | wc -l)"
	if [[ $amount -gt 4 ]]; then
		echo "More than 4 files (Amount is $amount), throttling requests that Virustotal API limit is not reached."
		throttle=15
	else
		throttle=0
	fi
	for file in "$SAMPLESPATH"/sample-source/pdf/*; do
		if [ ! -d "$file" ]; then
			sleep "$throttle"s
			echo "-------------------------------------------------------"
			echo Analysing: "${file##*/}"
			hash=$(sha256sum "${file}" | cut -d ' ' -f1)
			/usr/bin/python peepdf.py "$file" -f -c -g -j >"$SAMPLESPATH"/json-output/peepdf/peepdf_"$hash".json

			rate="$(jq '.peepdf_analysis.basic.detection.rate' "$SAMPLESPATH"/json-output/peepdf/peepdf_"$hash".json | awk -F'[ |/]' {'gsub(/"/, "", $1);print $1}')"
			if [ "$(
jq '.peepdf_analysis.basic.detection' "$SAMPLESPATH"/json-output/peepdf/peepdf_"$hash".json)" == "{}" ]; then
				echo "File not found on VirusTotal"

				# If file not found on Virustotal, copy to samples -folder
				cp "$file" "$SAMPLESPATH"/samples/
			else
				# File found on Virustotal, and considered malicious by at least some of the engines
				if [[ $rate  == 0 ]]; then
					# File found on Virustotal, but not considered malicious by any engine"
					echo "File found on Virustotal, not considered malicious by any engine"
					cp "$file" "$SAMPLESPATH"/samples/
				else
					echo "File found on VirusTotal, detected malicious by some engines"
					echo "Detected malicious by $rate engines"
					# echo "$hash" --- "${file##*/}" >>"$SAMPLESPATH"/peepdf-virustotal.log
					# Temporaly files to samples folder to test jsunpackn
					#cp "$file" "$SAMPLESPATH"/virustotal/
					cp "$file" "$SAMPLESPATH"/samples/
				fi
			fi
		fi
	done
	date >"$SAMPLESPATH"/output-files/_timestamp_
	# Copy logs to output
	# if (("$(find "$SAMPLESPATH"/*.log | wc -l)" != 0)); then echo "Copy logs" && cp "$SAMPLESPATH"/*.log "$SAMPLESPATH"/output-files/; fi

	if (("$(find "$SAMPLESPATH"/samples | wc -l)" != 0)); then
		mkdir -p "$SAMPLESPATH"/output-files/sample
		cp "$SAMPLESPATH"/samples/* "$SAMPLESPATH"/output-files/sample/
	fi
	if (("$(find "$SAMPLESPATH"/virustotal | wc -l)" != 0)); then
		cp -r "$SAMPLESPATH"/virustotal "$SAMPLESPATH"/output-files/
	fi
	if (("$(find "$SAMPLESPATH"/json-output/peepdf | wc -l)" != 0)); then
		mkdir -p "$SAMPLESPATH"/output-files/json-output/peepdf
		cp -r "$SAMPLESPATH"/json-output/peepdf "$SAMPLESPATH"/output-files/json-output
	fi
fi
