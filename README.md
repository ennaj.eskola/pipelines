## Concourse pipelines for the CinCan Project

Pipelines currently with easy setup for the pilot environment:   

[pdf-pipeline](pdf-pipeline)  
[document-pipeline](document-pipeline)  


## Descriptions


| Pipeline name                                   | Tools used                                                     | Description  |
|-------------------------------------------------|----------------------------------------------------------------|--------------|
| [cuckoo-pipeline](cuckoo-pipeline)              | Cuckoo sandbox                                                 | Cuckoo will provide a detailed report outlining the behavior of filea uploaded to Gitlab repository |
| [cortex_pipeline](cortex_pipeline)              | Cortex Abuse_Finder                         | Concourse pipeline that uses Cortex tool Abuse_Finder to analyze data such as IP, email or url |
| [document-pipeline](document-pipeline)          | ClamAV/PDFiD/PeePDF/JSunpack-n/shellcode/strings/oledump/olevba| The pipeline clones the samples from a Gitlab repo, sorts files to PDF and other documents and then runs appropriate tools to the sample files. * [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/document-pipeline.mp4)           |
| [email-pipeline](email-pipeline)                | Honeynet Thug / Pywhois                                        |              |
| [MISP-integration](MISP-integration)            |                                                                | Example script that uses MISP zmq to listen for events with relevant attachments that could be further analysed with some CinCan pipeline. |
| [pdf-pipeline](pdf-pipeline)                    | PDFiD / PeePDF / JSunpack-n / shellcode analysis.              | The pipeline polls for new files at a Gitlab repo, analyses the documents and writes the results to another branch of the repo. * [Watch the VIDEO](https://gitlab.com/CinCan/vault/blob/master/screencast/pdf-pipeline.mp4) |
| [smart-factory-18-pipeline](smart-factory-18-pipeline) | Strings / PE-scanner / Cortex                           | The pipeline tries to identify file type, then runs pe-scanner and strings, and finally Cortex results |
| [thug-pipeline](thug-pipeline)                  | Honeynet Thug                                                  | Run a honeyclient (thug) on each URL in a file, get the analysis files in a separate commit |
| [virustotal-pipeline](virustotal-pipeline)      | Suricata / iocextract / Virustotal                             | This pipeline consumes pcap files from s3 resource compatibible storage, archives pcaps and analyzes the files with suricata and virustotal. |
| [volatility-pipeline-1](volatility-pipeline-1)  | Volatility                                                     | Concourse pipeline that finds hidden processes and exports their executables to a git repo with Volatility. |

