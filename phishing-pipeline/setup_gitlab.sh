# Config
GITLAB_URL=172.20.0.5:5443
COMMON_NAME=ci.cincan.io
PIPELINE=phishing
SSH_KEY=gitlab_ssh_key

# Create a token
echo -e "Creating a personal access token for GitLab"
TOKEN=$(docker exec gitlab.cincan.io gitlab-rails runner -e production "\
        user = User.where(id: 1).first; \
        pat = PersonalAccessToken.new(user_id: user.id, name: 'api key', scopes: Gitlab::Auth::API_SCOPES, expires_at: 1.day.from_now); \
        pat.save!; \
        user.personal_access_tokens << pat; \
        puts pat.token;")
echo token: "$TOKEN"

ssh-keygen -f $SSH_KEY -t rsa -N ''
SSH_PUBLIC_KEY=$(cat $SSH_KEY.pub)

# Add key to root user inside GitLab container
docker exec gitlab.cincan.io curl -k --header "PRIVATE-TOKEN: $TOKEN" -X POST --data-urlencode "key=$SSH_PUBLIC_KEY" --data-urlencode "title=$PIPELINE" "https://$GITLAB_URL/api/v4/user/keys" --cacert /etc/ssl/certs/gitlab/$COMMON_NAME.crt --key /etc/ssl/certs/gitlab/$COMMON_NAME.key

# Create project inside GitLab container
docker exec gitlab.cincan.io curl -k --header "PRIVATE-TOKEN: $TOKEN" -X POST "https://$GITLAB_URL/api/v4/projects?name=$PIPELINE" --cacert /etc/ssl/certs/gitlab/$COMMON_NAME.crt --key /etc/ssl/certs/gitlab/$COMMON_NAME.key

