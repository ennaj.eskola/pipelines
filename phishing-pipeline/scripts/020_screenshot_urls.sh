#!/usr/bin/env sh

set -e -x

cp -r work-repo-screenshots/* work-repo-screenshot-output
urls=$PWD/work-repo-urls/found-urls-by-message
screenshots=$PWD/work-repo-screenshot-output/screenshots-by-message

cd work-repo-received/received
for url_id in *; do
   url_file="${urls}/${url_id}.json"
   screenshot_dir="${screenshots}/${url_id}"
   metadata="$screenshot_dir/metadata.json"

   test -f "$url_file" || continue
   if [ ! -f "$metadata" ]; then
      mkdir -p "$screenshot_dir"
      node /chrome/scrape-website.js \
	--url-file $url_file \
	--resolution 1366x768 \
	--output-har \
        --output-png \
        --output-dir ${screenshot_dir} > ${metadata} || true
   fi
done


