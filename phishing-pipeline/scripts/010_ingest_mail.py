#!/usr/bin/env python3

import os, sys, email, email.policy, re, json, email.utils

url_rx = re.compile(r'^(https?|ftp)://\S+')

def urls_from_msg(msg):
    plains = filter(lambda p: p.get_content_type() == 'text/plain', msg.walk())
    lines = []
    for part in plains:
        charset = part.get_content_charset() or 'iso-8859-1'        
        content = part.get_payload(decode=True).decode(charset, 'ignore')
        lines += content.splitlines()
    #print(lines)
    urls = list(filter(url_rx.match,
                       map(lambda x: x.strip(), lines)))
    return urls

def write_urls(path, urllist, request_msg):
    if not urllist:
        return False
    reply_address = request_msg.get('Reply-To') or request_msg.get('From')

    if not reply_address:
        return False
    with open(path, "w") as of:
        json.dump(dict(url_list=urllist,
                       reply_address=reply_address,
                       reply_subject='Re ' + request_msg.get("Subject", "(no subject)")),
                  of,
                  sort_keys=True,
                  indent=4)
    return True
    
def main():
    output_dir = "work-repo-output/found-urls-by-message"
    input_dir = "work-repo-received/received"
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    for fn in os.listdir(input_dir):
        output_path = os.path.join(output_dir, fn + '.json')
        input_path = os.path.join(input_dir, fn)
        # could test for existing output_path and skip here,
        # if only latest messages should be processed        
        with open(os.path.join(input_dir, fn), "rb") as fp:
            msg = email.message_from_binary_file(fp, policy=email.policy.default)
        url_list = urls_from_msg(msg)
        write_urls(os.path.join(output_dir, fn + '.json'),
                   url_list, msg)

if __name__ == '__main__':
    main()
