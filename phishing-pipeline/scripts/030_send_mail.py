import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

from email.utils import COMMASPACE, formatdate
from email import encoders, utils

import os, json, sys

### Util
import string
from urllib.parse import urlparse
import argparse

valid_chars = "-_.()%s%s" % (string.ascii_letters, string.digits)

def url_to_filename(url, whitelist=valid_chars, char_limit=255):
    purl = urlparse(url)
    filename = purl.netloc.encode('idna').decode('ascii')
    if purl.path:
       filename += purl.path.replace('/', '-')
    if purl.query:
        purl.query.replace('&', '-')
    cleaned_filename = ''.join(c for c in filename if c in whitelist)
    return cleaned_filename[:char_limit]

###

def whitelist_ok(to_field, whitelist_domains=[]):
    addrs = utils.getaddresses([to_field])
    if len(addrs) > 5:
        print("recipient list contains >5 addresses, not ok")
        return False

    for a in addrs:
        if not whitelist_ok_single(a, whitelist_domains):
            return False
    return True

def whitelist_ok_single(to_addr, whitelist_domains):
    realname, userdomainpart = utils.parseaddr(to_addr)
    domsplit = userdomainpart.split('@')
    if len(domsplit) != 2:
        # don't try to handle n@me@example.com style wise guys
        return False
    dom = domsplit[1]
    status = dom in whitelist_domains
    print("domain check for", to_addr, "->", status)
    return status

def send_mail(to, fro, subject, text, attachments=[], server="localhost"):
    if type(to) != list:
        to = [to]

    msg = MIMEMultipart()
    msg['From'] = fro
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.set_charset('utf-8')

    for attachment in attachments:
        if attachment.get('logs'):
            filename = url_to_filename(attachment['url']) + ".log"
            msg_log = MIMEText(attachment['logs'])
            msg_log.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(msg_log)
            msg.attach(MIMEText('Failed to fetch url: {}'.format(attachment['url'])))
            continue
        with open(attachment['png'], 'rb') as fp:
            filename = url_to_filename(attachment['url']) + ".png"
            msg_png = MIMEImage(fp.read())
            msg_png.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(msg_png)
        with open(attachment['har'], 'r') as fp:
            filename = url_to_filename(attachment['url']) + ".har"
            msg_har = MIMEText(fp.read())
            msg_har.add_header('Content-Disposition', "attachment; filename= %s" % filename)
            msg.attach(msg_har)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(fro, to, msg.as_string() )
    smtp.close()

def attach_file(msg, attachment, filetype):
    filename = url_to_filename(attachment['url']) + filetype
    part = MIMEBase('application', "octet-stream")
    part.set_payload( open(file,"rb").read() )
    Encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % filename)
    msg.attach(part)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--smtp-server", help="SMTP server to send mail from")
    parser.add_argument("--reply-whitelist", help="Sender domain whitelist. Only reply to users from these domains.")
    parser.add_argument("--reply-msg", help="Body of reply message")
    parser.add_argument("--email-sender", help="Sender name and address. Example: Bot McBotface <bot@cincan.io>")
    parser.add_argument("--url-dir", help="Where to read url data")
    parser.add_argument("--screenshot-dir", help="Where to read screenshot data")

    args = parser.parse_args()

    if not args.email_sender:
        sys.exit("Provide sender address")

    reply_whitelist = args.reply_whitelist.split(',')
    screenshot_dir = args.screenshot_dir
    urls_by_message = args.url_dir 

    if not os.path.exists(screenshot_dir):
    	sys.exit("Could not find screenshot directory {}".format(screenshot_dir))

    for dn in os.listdir(screenshot_dir):
        msg_dir = os.path.join(screenshot_dir, dn)
        stamp_path = os.path.join(msg_dir, "mailed-stamp")
        if os.path.exists(stamp_path):
            continue

        meta = json.load(open(os.path.join(urls_by_message, dn + '.json')))
        screens = []
        with open(os.path.join(msg_dir, 'metadata.json'), 'r') as json_string:
            for json_object in json_string:
                try:
                    print(json_object)
                    screens.append(json.loads(json_object))
                except Exception as e:
                    pass

        attachments = []
        for ss in screens:
            att = dict()
            if ss.get('url'):
                att['url'] = ss['url']
            if ss.get('png_output'):
                att['png'] = os.path.join(msg_dir, ss['png_output'])
            if ss.get('har_output'):
                att['har'] = os.path.join(msg_dir, ss['har_output'])
            if ss.get('logs'):
               att['logs'] = ss.get('logs')

            attachments.append(att)

        to_addrs = meta["reply_address"] # may have multiple in case of reply-to etc
        if not whitelist_ok(to_addrs, reply_whitelist):
            print("not sending mail because To-address contained "
                  "non-whitelisted recipients:", to_addrs)
            continue

        with open(stamp_path, "w") as stamp_fp:
            stamp_fp.write("mailed\n")
            send_mail(to_addrs, args.email_sender, meta['reply_subject'], args.reply_msg, attachments=attachments, server=args.smtp_server)

if __name__ == '__main__':
    main()
